export default defineNuxtConfig({
    runtimeConfig: {
        public: {
            apiBaseUrl: 'NUXT_PUBLIC_API_BASE_URL',
        }
    },
    devtools: { enabled: true },
    buildModules: ['@nuxt/vuex',"@pinia/nuxt"],
    css: ['~/assets/css/main.css','~/assets/css/admin.css'],
    modules: ["@pinia/nuxt","@pinia/nuxt"]
})